from setuptools import find_packages, setup

setup(
    name='banana',
    version="0.0.1",
    package_dir={"": "build"},
    packages=find_packages(where="build"),
    author='Luca Soato',
    author_email='info@lucasoato.it',
    description='Prova Projects',
    entry_points={
        'console_scripts': [
            'banana = banana.__init__:banana'
        ],
    }
)
